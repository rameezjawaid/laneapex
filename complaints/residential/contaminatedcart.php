<div class="container container-inner screen-full screen-overflow col-md-12 col-sm-12 col-xm-12" id="container-contaminatedcart">
  <div class="header">
    <a href="javascript: void(0)" class="close-me"> <i class="glyphicon glyphicon-menu-left"> </i> </a>
    <span> Contaminated Cart </span>
  </div>

  <!-- content -->
  <div class="page-content col-md-12 col-sm-12 col-xm-12">
    <!-- overflowed content -->
    <div class="overflowed-content">
      
      <div class="col-md-12 col-sm-12 col-xm-12 inner-fields inner-fields-radio">
        <div class="radio radio-inline">
            <input type="radio" name="residentials_contaminatedcart" id="residentials_contaminatedcart_1" value="option1"   >
            <label for="residentials_contaminatedcart_1" class="clr-blue">
                Y/D as Trash
            </label>
        </div>
      </div>

      <div class="col-md-12 col-sm-12 col-xm-12 inner-fields inner-fields-radio">
        <div class="radio radio-inline">
            <input type="radio" name="residentials_contaminatedcart" id="residentials_contaminatedcart_2" value="option1">
            <label for="residentials_contaminatedcart_2" class="clr-blue">
                R/R as Trash
            </label>
        </div>
      </div>

      <div class="col-md-12 col-sm-12 col-xm-12 inner-fields inner-fields-radio">
        <div class="radio radio-inline">
            <input type="radio" name="residentials_contaminatedcart" id="residentials_contaminatedcart_3" value="option1">
            <label for="residentials_contaminatedcart_3" class="clr-blue">
                Oil
            </label>
        </div>
      </div>

      <div class="col-md-12 col-sm-12 col-xm-12 inner-fields inner-fields-radio">
        <div class="radio radio-inline">
            <input type="radio" name="residentials_contaminatedcart" id="residentials_contaminatedcart_4" value="option1">
            <label for="residentials_contaminatedcart_4" class="clr-blue">
                Paint
            </label>
        </div>
      </div>
    </div>
    <!-- overflowed content -->
  </div>
  <!-- content -->
</div>