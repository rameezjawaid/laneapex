<!-- extra -->
<div class="container container-inner screen-full screen-overflow col-md-12 col-sm-12 col-xm-12" id="container-extra">
  <div class="header">
    <a href="javascript: void(0)" class="close-me"> <i class="glyphicon glyphicon-menu-left"> </i> </a>
    <span> Extras </span>
  </div>

  <!-- content -->
  <div class="page-content col-md-12 col-sm-12 col-xm-12">
    <!-- overflowed content -->
    <div class="overflowed-content">
      <div class="col-md-12 col-sm-12 col-xm-12 inner-fields">
          <label class="pull-left"> Ex. Bag </label>
          <input type="text" name="residentials_extra" class="pull-right" />
      </div>

      <div class="col-md-12 col-sm-12 col-xm-12 inner-fields">
          <label class="pull-left"> Ex. Can </label>
          <input type="text" name="residentials_extra" class="pull-right" />
      </div>

      <div class="col-md-12 col-sm-12 col-xm-12 inner-fields">
          <label class="pull-left"> Misc. Extra </label>
          <input type="text" name="residentials_extra" class="pull-right" />
      </div>

      <div class="col-md-12 col-sm-12 col-xm-12 inner-fields">
          <label class="pull-left"> Reload R/C </label>
          <input type="text" name="residentials_extra" class="pull-right" />
      </div>

      <div class="col-md-12 col-sm-12 col-xm-12 inner-fields inner-fields-radio">
        <div class="radio radio-inline">
            <input type="radio" name="residentials_extra" id="ex-95" value="option1"   >
            <label for="ex-95" class="clr-blue">
                Ex 95 R/C
            </label>
        </div>
      </div>

      <div class="col-md-12 col-sm-12 col-xm-12 inner-fields inner-fields-radio">
        <div class="radio radio-inline">
            <input type="radio" name="residentials_extra" id="ex-45" value="option1">
            <label for="ex-45" class="clr-blue">
                Ex 45 gal can
            </label>
        </div>
      </div>
    </div>
    <!-- overflowed content -->
  </div>
  <!-- content -->
</div>