<div class="container container-inner screen-full screen-overflow col-md-12 col-sm-12 col-xm-12" id="container-notout">
  <div class="header">
    <a href="javascript: void(0)" class="close-me"> <i class="glyphicon glyphicon-menu-left"> </i> </a>
    <span> Not Out </span>
  </div>

  <!-- content -->
  <div class="page-content col-md-12 col-sm-12 col-xm-12">
    <!-- overflowed content -->
    <div class="overflowed-content">
      <div class="col-md-12 col-sm-12 col-xm-12 inner-fields field-checkbox">
          <label class="pull-left" for="residentials_notout"> Not Out </label>
          <input type="checkbox" name="residentials_notout" id="residentials_notout" class="pull-right" />
      </div>
    </div>
    <!-- overflowed content -->
  </div>
  <!-- content -->
</div>