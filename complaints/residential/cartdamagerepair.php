<div class="container container-inner screen-full screen-overflow col-md-12 col-sm-12 col-xm-12" id="container-cartdamagerepair">
  <div class="header">
    <a href="javascript: void(0)" class="close-me"> <i class="glyphicon glyphicon-menu-left"> </i> </a>
    <span> Cart Damage Repair </span>
  </div>

  <!-- content -->
  <div class="page-content col-md-12 col-sm-12 col-xm-12">
    <!-- overflowed content -->
    <div class="overflowed-content">
      <div class="col-md-12 col-sm-12 col-xm-12 inner-fields inner-fields-radio">
        <div class="radio radio-inline">
            <input type="radio" name="residentials_cartdamagerepair" id="residentials_cartdamagerepair_1" value="option1">
            <label for="residentials_cartdamagerepair_1" class="clr-blue">
                Wheel Repaired
            </label>
        </div>
      </div>
      <div class="col-md-12 col-sm-12 col-xm-12 inner-fields inner-fields-radio">
        <div class="radio radio-inline">
            <input type="radio" name="residentials_cartdamagerepair" id="residentials_cartdamagerepair_2" value="option1">
            <label for="residentials_cartdamagerepair_2" class="clr-blue">
                Replace cart wheel
            </label>
        </div>
      </div>
      <div class="col-md-12 col-sm-12 col-xm-12 inner-fields inner-fields-radio">
        <div class="radio radio-inline">
            <input type="radio" name="residentials_cartdamagerepair" id="residentials_cartdamagerepair_3" value="option1">
            <label for="residentials_cartdamagerepair_3" class="clr-blue">
                R/C damage customer Cart Broken 
            </label>
        </div>
      </div>

      <div class="col-md-12 col-sm-12 col-xm-12 inner-fields inner-fields-radio">
        <div class="radio radio-inline">
            <input type="radio" name="residentials_cartdamagerepair" id="residentials_cartdamagerepair_4" value="option1">
            <label for="residentials_cartdamagerepair_4" class="clr-blue">
                R/C damage customer Lost in Packer
            </label>
        </div>
      </div>

      <div class="col-md-12 col-sm-12 col-xm-12 inner-fields inner-fields-radio">
        <div class="radio radio-inline">
            <input type="radio" name="residentials_cartdamagerepair" id="residentials_cartdamagerepair_5" value="option1">
            <label for="residentials_cartdamagerepair_5" class="clr-blue">
                Replace cart
            </label>
        </div>
      </div>
    </div>
    <!-- overflowed content -->
  </div>
  <!-- content -->
</div>