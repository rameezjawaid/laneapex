<div class="container container-inner screen-full screen-overflow col-md-12 col-sm-12 col-xm-12" id="container-overflow">
  <div class="header">
    <a href="javascript: void(0)" class="close-me"> <i class="glyphicon glyphicon-menu-left"> </i> </a>
    <span> Overflow </span>
  </div>

  <!-- content -->
  <div class="page-content col-md-12 col-sm-12 col-xm-12">
    <!-- overflowed content -->
    <div class="overflowed-content">
      <div class="col-md-12 col-sm-12 col-xm-12 inner-fields inner-fields-radio">
        <div class="radio radio-inline">
            <input type="radio" name="residentials_overflow" id="residentials_overflow_1" value="option1"   >
            <label for="residentials_overflow_1" class="clr-blue">
              O/F 35
            </label>
        </div>
      </div>

      <div class="col-md-12 col-sm-12 col-xm-12 inner-fields inner-fields-radio">
        <div class="radio radio-inline">
            <input type="radio" name="residentials_overflow" id="residentials_overflow_2" value="option1">
            <label for="residentials_overflow_2" class="clr-blue">
                O/F 65
            </label>
        </div>
      </div>

      <div class="col-md-12 col-sm-12 col-xm-12 inner-fields inner-fields-radio">
        <div class="radio radio-inline">
            <input type="radio" name="residentials_overflow" id="residentials_overflow_3" value="option1">
            <label for="residentials_overflow_3" class="clr-blue">
                O/F 95
            </label>
        </div>
      </div>
    </div>
    <!-- overflowed content -->
  </div>
  <!-- content -->
</div>