<div class="container container-inner screen-full screen-overflow col-md-12 col-sm-12 col-xm-12" id="container-skippedservice">
  <div class="header">
    <a href="javascript: void(0)" class="close-me"> <i class="glyphicon glyphicon-menu-left"> </i> </a>
    <span> Skipped Service </span>
  </div>

  <!-- content -->
  <div class="page-content col-md-12 col-sm-12 col-xm-12">
    <!-- overflowed content -->
    <div class="overflowed-content">

      <div class="col-md-12 col-sm-12 col-xm-12 inner-fields inner-fields-radio">
        <div class="radio radio-inline">
            <input type="radio" name="residentials_skippedservice" id="residentials_skippedservice_1" value="option1"   >
            <label for="residentials_skippedservice_1" class="clr-blue">
                Cart too Heavy
            </label>
        </div>
      </div>

      <div class="col-md-12 col-sm-12 col-xm-12 inner-fields inner-fields-radio">
        <div class="radio radio-inline">
            <input type="radio" name="residentials_skippedservice" id="residentials_skippedservice_2" value="option1">
            <label for="residentials_skippedservice_2" class="clr-blue">
                Bulk Items Left
            </label>
        </div>
      </div>

      <div class="col-md-12 col-sm-12 col-xm-12 inner-fields inner-fields-radio">
        <div class="radio radio-inline">
            <input type="radio" name="residentials_skippedservice" id="residentials_skippedservice_3" value="option1">
            <label for="residentials_skippedservice_3" class="clr-blue">
                Temp. Blocked
            </label>
        </div>
      </div>
    </div>
    <!-- overflowed content -->
  </div>
  <!-- content -->
</div>