<div class="container container-inner screen-full screen-overflow col-md-12 col-sm-12 col-xm-12" id="container-other">
  <div class="header">
    <a href="javascript: void(0)" class="close-me"> <i class="glyphicon glyphicon-menu-left"> </i> </a>
    <span> Other </span>
  </div>

  <!-- content -->
  <div class="page-content col-md-12 col-sm-12 col-xm-12">
    <!-- overflowed content -->
    <div class="overflowed-content">
    
      <div class="col-md-12 col-sm-12 col-xm-12 inner-fields inner-fields-radio">
        <div class="radio radio-inline">
            <input type="radio" name="residentials_other" id="residentials_other_1" value="option1"   >
            <label for="residentials_other_1" class="clr-blue">
                Trash on Ground
            </label>
        </div>
      </div>

      <div class="col-md-12 col-sm-12 col-xm-12 inner-fields inner-fields-radio">
        <div class="radio radio-inline">
            <input type="radio" name="residentials_other" id="residentials_other_2" value="option1">
            <label for="residentials_other_2" class="clr-blue">
                Low wires
            </label>
        </div>
      </div>

      <div class="col-md-12 col-sm-12 col-xm-12 inner-fields inner-fields-radio">
        <div class="radio radio-inline">
            <input type="radio" name="residentials_other" id="residentials_other_3" value="option1">
            <label for="residentials_other_3" class="clr-blue">
                Spilled Trash
            </label>
        </div>
      </div>

      <div class="col-md-12 col-sm-12 col-xm-12 inner-fields inner-fields-radio">
        <div class="radio radio-inline">
            <input type="radio" name="residentials_other" id="residentials_other_4" value="option1">
            <label for="residentials_other_4" class="clr-blue">
                Relocate Cart
            </label>
        </div>
      </div>
    </div>
    <!-- overflowed content -->
  </div>
  <!-- content -->
</div>