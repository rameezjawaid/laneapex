<div class="container container-inner screen-full screen-overflow col-md-12 col-sm-12 col-xm-12" id="container-containerissue">
  <div class="header">
    <a href="javascript: void(0)" class="close-me"> <i class="glyphicon glyphicon-menu-left"> </i> </a>
    <span> Container Issue </span>
  </div>

  <!-- content -->
  <div class="page-content col-md-12 col-sm-12 col-xm-12">
    <!-- overflowed content -->
    <div class="overflowed-content">
      <div class="col-md-12 col-sm-12 col-xm-12 inner-fields inner-fields-radio">
        <div class="radio radio-inline">
            <input type="radio" name="commercial_containerissue" id="commercial_containerissue_1" value="option1">
            <label for="commercial_containerissue_1" class="clr-blue">
                Repair / Clean
            </label>
        </div>
      </div>
      <div class="col-md-12 col-sm-12 col-xm-12 inner-fields inner-fields-radio">
        <div class="radio radio-inline">
            <input type="radio" name="commercial_containerissue" id="commercial_containerissue_2" value="option1">
            <label for="commercial_containerissue_2" class="clr-blue">
                Wheel Repair or Damage
            </label>
        </div>
      </div>
      <div class="col-md-12 col-sm-12 col-xm-12 inner-fields inner-fields-radio">
        <div class="radio radio-inline">
            <input type="radio" name="commercial_containerissue" id="commercial_containerissue_3" value="option1"   >
            <label for="residentials_cartdamagerepair_3" class="clr-blue">
                Customer or Enclosure Damage 
            </label>
        </div>
      </div>
    </div>
    <!-- overflowed content -->
  </div>
  <!-- content -->
</div>