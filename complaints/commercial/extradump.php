<!-- extra -->
<div class="container container-inner screen-full screen-overflow col-md-12 col-sm-12 col-xm-12" id="container-extradump">
  <div class="header">
    <a href="javascript: void(0)" class="close-me"> <i class="glyphicon glyphicon-menu-left"> </i> </a>
    <span> Extra Dump </span>
  </div>

  <!-- content -->
  <div class="page-content col-md-12 col-sm-12 col-xm-12">
    <!-- overflowed content -->
    <div class="overflowed-content">
      <div class="col-md-12 col-sm-12 col-xm-12 inner-fields">
          <label class="pull-left"> Time Reloading </label>
          <input type="text" name="commercial_extradump" class="pull-right" />
      </div>

      <div class="col-md-12 col-sm-12 col-xm-12 inner-fields">
          <label class="pull-left"> Overweight </label>
          <input type="text" name="commercial_extradump" class="pull-right" />
      </div>

      <div class="col-md-12 col-sm-12 col-xm-12 inner-fields inner-fields-radio">
        <div class="radio radio-inline">
            <input type="radio" name="commercial_extradump" id="commercial_extradump_radio1" value="option1">
            <label for="commercial_extradump_radio1" class="clr-blue">
                Ex. Dump Size 1 Yard
            </label>
        </div>
      </div>

      <div class="col-md-12 col-sm-12 col-xm-12 inner-fields inner-fields-radio">
        <div class="radio radio-inline">
            <input type="radio" name="commercial_extradump" id="commercial_extradump_radio2" value="option1">
            <label for="commercial_extradump_radio2" class="clr-blue">
                Ex. Dump Size 1.5 Yard
            </label>
        </div>
      </div>

      <div class="col-md-12 col-sm-12 col-xm-12 inner-fields inner-fields-radio">
        <div class="radio radio-inline">
            <input type="radio" name="commercial_extradump" id="commercial_extradump_radio3" value="option1">
            <label for="commercial_extradump_radio3" class="clr-blue">
                Ex. Dump Size 2 Yard
            </label>
        </div>
      </div>

      <div class="col-md-12 col-sm-12 col-xm-12 inner-fields inner-fields-radio">
        <div class="radio radio-inline">
            <input type="radio" name="commercial_extradump" id="commercial_extradump_radio4" value="option1">
            <label for="commercial_extradump_radio4" class="clr-blue">
                Ex. Dump Size 3 Yard
            </label>
        </div>
      </div>

      <div class="col-md-12 col-sm-12 col-xm-12 inner-fields inner-fields-radio">
        <div class="radio radio-inline">
            <input type="radio" name="commercial_extradump" id="commercial_extradump_radio5" value="option1">
            <label for="commercial_extradump_radio5" class="clr-blue">
                Ex. Dump Size 4 Yard
            </label>
        </div>
      </div>

      <div class="col-md-12 col-sm-12 col-xm-12 inner-fields inner-fields-radio">
        <div class="radio radio-inline">
            <input type="radio" name="commercial_extradump" id="commercial_extradump_radio6" value="option1">
            <label for="commercial_extradump_radio6" class="clr-blue">
                Ex. Dump Size 5 Yard
            </label>
        </div>
      </div>

      <div class="col-md-12 col-sm-12 col-xm-12 inner-fields inner-fields-radio">
        <div class="radio radio-inline">
            <input type="radio" name="commercial_extradump" id="commercial_extradump_radio7" value="option1">
            <label for="commercial_extradump_radio7" class="clr-blue">
                Ex. Dump Size 6 Yard
            </label>
        </div>
      </div>
    </div>
    <!-- overflowed content -->
  </div>
  <!-- content -->
</div>