<div class="container container-inner screen-full screen-overflow col-md-12 col-sm-12 col-xm-12" id="container-overfilled">
  <div class="header">
    <a href="javascript: void(0)" class="close-me"> <i class="glyphicon glyphicon-menu-left"> </i> </a>
    <span> Overfilled </span>
  </div>

  <!-- content -->
  <div class="page-content col-md-12 col-sm-12 col-xm-12">
    <!-- overflowed content -->
    <div class="overflowed-content">
      <div class="col-md-12 col-sm-12 col-xm-12 inner-fields field-checkbox">
          <label class="pull-left" for="commercial_overfilled"> Overfilled </label>
          <input type="checkbox" name="commercial_overfilled" id="commercial_overfilled" class="pull-right" />
      </div>
    </div>
    <!-- overflowed content -->
  </div>
  <!-- content -->
</div>