<div class="container container-inner screen-full screen-overflow col-md-12 col-sm-12 col-xm-12" id="container-serviceblocked">
  <div class="header">
    <a href="javascript: void(0)" class="close-me"> <i class="glyphicon glyphicon-menu-left"> </i> </a>
    <span> Service Blocked </span>
  </div>

  <!-- content -->
  <div class="page-content col-md-12 col-sm-12 col-xm-12">
    <!-- overflowed content -->
    <div class="overflowed-content">
      
      <div class="col-md-12 col-sm-12 col-xm-12 inner-fields inner-fields-radio">
        <div class="radio radio-inline">
            <input type="radio" name="commercial_serviceblocked" id="commercial_serviceblocked_1" value="option1"   >
            <label for="commercial_serviceblocked_1" class="clr-blue">
                Street Closed
            </label>
        </div>
      </div>

      <div class="col-md-12 col-sm-12 col-xm-12 inner-fields inner-fields-radio">
        <div class="radio radio-inline">
            <input type="radio" name="commercial_serviceblocked" id="commercial_serviceblocked_2" value="option1"   >
            <label for="commercial_serviceblocked_2" class="clr-blue">
                Low Wires
            </label>
        </div>
      </div>

      <div class="col-md-12 col-sm-12 col-xm-12 inner-fields inner-fields-radio">
        <div class="radio radio-inline">
            <input type="radio" name="commercial_serviceblocked" id="commercial_serviceblocked_3" value="option1"   >
            <label for="commercial_serviceblocked_3" class="clr-blue">
                Vehicle
            </label>
        </div>
      </div>
    </div>
    <!-- overflowed content -->
  </div>
  <!-- content -->
</div>