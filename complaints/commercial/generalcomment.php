<div class="container container-inner screen-full screen-overflow col-md-12 col-sm-12 col-xm-12" id="container-generalcomment">
  <div class="header">
    <a href="javascript: void(0)" class="close-me"> <i class="glyphicon glyphicon-menu-left"> </i> </a>
    <span> General Comment </span>
  </div>

  <!-- content -->
  <div class="page-content col-md-12 col-sm-12 col-xm-12">
    <!-- overflowed content -->
    <div class="overflowed-content">
      <div class="col-md-12 col-sm-12 col-xm-12 inner-fields">
          <textarea name="commercial_generalcomment" class="comments"></textarea>
      </div>
    </div>
    <!-- overflowed content -->
  </div>
  <!-- content -->
</div>