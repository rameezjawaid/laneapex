<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title> Lane Apex </title>

    <?php require 'header_assets.php'; ?>

  </head>
  <body class="screen-main">
    <div class="container screen-full col-md-3 col-sm-12 col-xm-12">
      <!-- splash item -->
      <img src="images/head-logo.jpg" class="head-logo" />
      <img src="images/ft-logo.jpg" class="ft-logo" />
      <!-- splash item -->
      
      <!-- loader -->
      <div class="spinner">
        <div class="bounce1"></div>
        <div class="bounce2"></div>
        <div class="bounce3"></div>
      </div>
      <!-- loader -->

      <!-- login items -->
      <div id="form-login-wrap" class="col-md-12 col-sm-8 form-login-wrap">
        <form class="form-signin" action="search.php" method="POST">
          <input type="text" name="user" class="form-control" placeholder="User ID" />
          <input type="password" name="password" class="form-control" placeholder="Password"/>
          <div class="btn-blue-wrapper">
            <input type="submit" name="btn-login" value="Login" id="btn-login" class="btn-blue" />

            <div class="spinner">
              <div class="bounce1"></div>
              <div class="bounce2"></div>
              <div class="bounce3"></div>
            </div>
          </div>
        </form>
      </div>
      <!-- login items -->
    </div>

    <?php require 'footer_assets.php'; ?>
  </body>
</html>