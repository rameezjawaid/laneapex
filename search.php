<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title> Search </title>
    
    <?php require 'header_assets.php'; ?>

  </head>
  <body class="page-search">
    <div class="container screen-full col-md-3 col-sm-12 col-xm-12">
      <div class="header">
        <a href="index.php" class="btn-small"> <i class="glyphicon glyphicon-off"> </i> Log Out </a>
        <span> Search </span>
      </div>

      <!-- content -->
      <div class="page-content col-md-12 col-sm-12 col-xm-12">
        <div class="pull-left res-com">
          <div class="radio radio-inline">
              <input type="radio" name="radio2" id="residential" value="residential"   >
              <label for="residential" class="clr-blue">
                  Residential
              </label>
          </div>

          <div class="radio radio-inline">
              <input type="radio" name="radio2" id="commercial" value="commercial">
              <label for="commercial" class="clr-blue">
                  Commercial
              </label>
          </div>
        </div>

        <!-- location -->
          <!-- gps -->
          <div class="gps-located">
            <span> GPS location </span>
            <div class="btn-blue-wrapper location-detecting">
              <a href="javascript: void(0)" id="btn-getlocation" class="btn-blue"> Detecting Location </a>
              <div class="spinner">
                <div class="bounce1"></div>
                <div class="bounce2"></div>
                <div class="bounce3"></div>
              </div>

              <div class="location-detected-text">
                <i class="glyphicon glyphicon-map-marker"></i>
                <em>G 58/9 Malir Colony, Karachi, Pakistan</em>

                <input type="hidden" name="lat" id="field-latitude" />
                <input type="hidden" name="lat" id="field-longitude" />
              </div>
            </div>
          </div>

          <div class="seperator"> <span> OR </span></div>
          <!-- gps -->

          <!-- address -->
          <div class="given-adrress">
            <span> Type a street address </span>

            <input type="text" name="address_street" id="address_street" class="general-fields" placeholder="Enter Street Number" />
            <input type="text" name="address_name" id="address_name" class="general-fields" placeholder="Enter Street Name" />
            <select name="address_suffix" id="address_suffix" class="general-fields">
              <option> Please Select Suffix </option>
              <option> CA </option>
              <option> SC </option>
              <option> HU </option>
            </select>
            
            <a href="complaint-residential.php" id="page-next" class="btn-blue"> Next </a>
          </div>
          <!-- address -->
        <!-- location -->
      </div>
      <!-- content -->
    </div>

    <?php require 'footer_assets.php'; ?>

  </body>
</html>