<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title> Complaint </title>

    <?php require 'header_assets.php'; ?>

  </head>
  <body class="page-inner-commercial">
    <div class="container screen-full screen-overflow col-md-12 col-sm-12 col-xm-12">
      <div class="header">
        <a href="search.php" > <i class="glyphicon glyphicon-menu-left"> </i> </a>
        <span> Commercial </span>
      </div>

      <!-- content -->
      <div class="page-content col-md-12 col-sm-12 col-xm-12">
        <!-- overflowed content -->
        <div class="overflowed-content">
          <div class="file-upload-wrap">
            <label for="files"> <!-- <i class="glyphicon glyphicon-camera" aria-hidden="true"></i> --> </label>
            <input id="files" type="file" multiple/>
            <div id="result-wrapper"> <output id="img-result"> </output> </div>
          </div>

          <ul class="inner-list-wrap">
            <li> <a href="javascript: void(0)" id="btn-extradump"> Extra Dump <i class="glyphicon glyphicon-menu-right"> </i> </a> </li>
            <li> <a href="javascript: void(0)" id="btn-banneditems"> Banned Item <i class="glyphicon glyphicon-menu-right"> </i> </a> </li>
            <li> <a href="javascript: void(0)" id="btn-containerissue"> Container Issue <i class="glyphicon glyphicon-menu-right"> </i> </a> </li>
            <li> <a href="javascript: void(0)" id="btn-serviceblocked"> Service Blocked <i class="glyphicon glyphicon-menu-right"> </i> </a> </li>
            <li> <a href="javascript: void(0)" id="btn-trashonground"> Trash on Ground <i class="glyphicon glyphicon-menu-right"> </i> </a> </li>
            <li> <a href="javascript: void(0)" id="btn-overfilled"> Overfilled <i class="glyphicon glyphicon-menu-right"> </i> </a> </li>
            <li> <a href="javascript: void(0)" id="btn-gatelocked"> Gate Locked <i class="glyphicon glyphicon-menu-right"> </i> </a> </li>
            <li> <a href="javascript: void(0)" id="btn-notout"> Not Out <i class="glyphicon glyphicon-menu-right"> </i> </a> </li>
            <li> <a href="javascript: void(0)" id="btn-generalcomment">General Comment <i class="glyphicon glyphicon-menu-right"> </i> </a> </li>
            <li><a href="#" id="page-submit" class="btn-blue"> Submit </a></li>
          </ul>

        </div>
        <!-- overflowed content -->
      </div>
      <!-- content -->
    </div>

    <!-- inner sub pages -->
      <?php
        $arr = array('extradump','banneditems','containerissue','serviceblocked','trashonground','overfilled','gatelocked','notout','generalcomment');
        foreach ($arr as $key => $value)
        {
          //$id = $value;
          require 'complaints/commercial/'.$value.'.php';  
        }
      ?>
    <!-- inner sub pages -->
    
    <?php require 'footer_assets.php'; ?>

  </body>
</html>