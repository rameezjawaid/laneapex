jQuery('document').ready(function() {

    var latitude, longitude;

    function getLocation() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(showPosition);
        } else {
            alert("Geolocation is not supported by this browser.");
        }
    }
    function showPosition(position) {
        latitude = position.coords.latitude;
        longitude = position.coords.longitude;
        // alert("Latitude: " + position.coords.latitude +" Longitude: " + position.coords.longitude);
    }

    getLocation();

    jQuery('#btn-getlocation').click(function() {

        // enabel loader
        jQuery(this).parent().addClass('location-detecting');

        jQuery.ajax({
            url: 'dummy.php',
            type: "POST",
            data: {
                'lat': latitude,
                'long': longitude
            },
            success: function(data, status) {
                // var data = jQuery.parseJSON(data);
                // if (typeof data == 'object') {
                		jQuery('.location-detected-text em').html(data);

                		setTimeout(function()
                		{
	                		jQuery('#btn-getlocation').parent().addClass('location-detected');
              		}, 1000);
                //     //console.log(data.results[0].formatted_address);
                // }
                console.log(data);
            },
            error: function(xhr, desc, err) {
            		jQuery('.location-detected-text em').html('Unable to locate.');
                console.log(xhr);
                console.log("Desc: " + desc + "\nErr:" + err);
            }
        });

    });

    jQuery('.res-com input').on('change', function()
    {
     switch($(this).val()) {
         case 'residential':
             jQuery('#page-next').attr('href','complaint-residential.php')
             break;
         case 'commercial':
             jQuery('#page-next').attr('href','complaint-commercial.php')
             break;
     }
		});

    // auto detect location
    setTimeout(function(){

    jQuery('#btn-getlocation').trigger('click');
    }, 2000);

});

jQuery(window).load(function() {
    setTimeout(function() {
        if (jQuery('body').hasClass('screen-main'))
            jQuery('.container').toggleClass('screen-splash');

    }, 0);


    setTimeout(function() {
        if (jQuery('body').hasClass('screen-main'))
            jQuery('.container').toggleClass('screen-login');

    }, 3000);


    // innerpage functionality
    if (jQuery('body').hasClass('page-inner-residentials') || jQuery('body').hasClass('page-inner-commercial')) {
        jQuery('.inner-list-wrap li a').on('click', function() {
            var id = jQuery(this).attr('id');
            id = id.split('-');
            id = id[1];

            jQuery('#container-' + id).css('display', 'block');
            // alert(id);
        });

        jQuery('.close-me').click(function() {
            jQuery(this).parent().parent().css('display', 'none');
        })
    }


    //show "edited info text" if someone edit/update the fields
    jQuery('.inner-fields input, .inner-fields textarea').change(function()
    {
    	var id = jQuery(this).attr('name')
  	  id = id.split('_');
    	var page_ = id[0];
    	var list_ = id[1];
    	jQuery('.page-inner-'+page_+' #btn-'+list_).parent().addClass('edited');
    });

    // wrap label text within span
    // jQuery('.inner-fields label').html('<span>'+jQuery('.inner-fields label').html()+'<span>');
    jQuery('.inner-fields label').each(function(index,element)
    {
    	jQuery(this).html('<span>'+jQuery(this).html()+'<span>');
    });


});

var upload_count = 0;

window.onload = function() {
    //Check File API support
    if (window.File && window.FileList && window.FileReader) {
        var filesInput = document.getElementById("files");
        filesInput.addEventListener("change", function(event) {
            var files = event.target.files; //FileList object
            var output = document.getElementById("img-result");
            for (var i = 0; i < files.length; i++) {
                var file = files[i];
                //Only pics
                if (!file.type.match('image'))
                    continue;
                var picReader = new FileReader();
                picReader.addEventListener("load", function(event) {
                    var picFile = event.target;
                    upload_count++;
                    var div = document.createElement("div");
                    div.className = "img-loader"+upload_count;
                    div.innerHTML = "<i class='fa fa-times' aria-hidden='true'></i><img class='thumbnail' src='" + picFile.result + "'" +
                        "title='" + picFile.name + "'/>";
                    div.onclick = test;

                    document.getElementById("img-result").style.width = 90*upload_count+"px";
                    output.insertBefore(div, null);
                });
                //Read the image
                picReader.readAsDataURL(file);
            }
        });
    } else {
        console.log("Your browser does not support File API");
    }
}

// delete image
function test()
{
	var r = confirm("Are you sure you want to Delete?");
	if (r == true)
	{
		upload_count--;
		jQuery('#img-result').css('width', (jQuery('#img-result').width()-90)+'px');
		jQuery('.'+jQuery(this).attr('class')).remove();
	}
	else
	{

	}
}